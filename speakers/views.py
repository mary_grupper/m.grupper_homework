from django.shortcuts import render

from .models import Speakers
def index(request):
    speakers_list = Speakers.objects.all()
    context = {'speakers': speakers_list}
    return render(request, 'speakers/index.html', context)
