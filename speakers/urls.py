from django.conf.urls import patterns, url
from speakers import views
urlpatterns = patterns('',
    url(r'^$', views.index, name='speakers_index'),
)