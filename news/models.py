from django.db import models

class News(models.Model):
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    image = models.ImageField(null=True, blank=True)
    pub_date = models.DateTimeField()
