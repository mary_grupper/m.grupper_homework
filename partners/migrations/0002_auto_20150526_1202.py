# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partners',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='partners',
            name='text',
        ),
        migrations.AddField(
            model_name='partners',
            name='image',
            field=models.ImageField(default=datetime.datetime(2015, 5, 26, 9, 2, 18, 39000, tzinfo=utc), upload_to=b''),
            preserve_default=False,
        ),
    ]
