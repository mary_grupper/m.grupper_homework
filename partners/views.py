from django.shortcuts import render

from .models import Partners
def index(request):
    partners_list = Partners.objects.all()
    context = {'partners': partners_list}
    return render(request, 'partners/index.html', context)
