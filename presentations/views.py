# -*- coding: utf-8 -*- 
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Presentations
from .forms import PresentationsForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index1(request):
    presentations_list = Presentations.objects.filter(is_public=True)
    context = {'presentations': presentations_list}
    return render(request, 'presentations/index.html', context)

def edit(request, presentations_id):
    presentation = Presentations.objects.get(id = presentations_id)
    if request.method == "POST":
        form = PresentationsForm(request.POST, instance = presentation)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect( reverse('presentations_index'))

    form = PresentationsForm(instance = presentation)
    context = {'form': form}
    return render(request, 'presentations/edit.html', context)
    

def add(request):
    if request.method == 'POST':
        form = PresentationsForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect( reverse('presentations_index'))
       
    else:
        form = PresentationsForm()
    context = {'form':form}
    return render(request, 'presentations/add.html', context)
            
def delete (request, presentations_id):
    presentation = Presentations.objects.get(id = presentations_id)
    presentation.delete()
    return HttpResponseRedirect(reverse('presentations_index'))
    
def index(request):
    presentations_list = Presentations.objects.all()
    paginator = Paginator(presentations_list, 8)

    page = request.GET.get('page')
    try:
        presentations = paginator.page(page)
    except PageNotAnInteger:
        presentations = paginator.page(1)
    except EmptyPage:
        presentations = paginator.page(paginator.num_pages)

    context = {'presentations': presentations}
    return render(request, 'presentations/index.html', context)