# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0004_presentations_is_public'),
    ]

    operations = [
        migrations.AlterField(
            model_name='presentations',
            name='is_public',
            field=models.BooleanField(default=True),
        ),
    ]
