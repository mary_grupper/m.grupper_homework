# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentations',
            name='name',
            field=models.CharField(default=datetime.datetime(2015, 5, 26, 9, 18, 16, 7000, tzinfo=utc), max_length=70),
            preserve_default=False,
        ),
    ]
