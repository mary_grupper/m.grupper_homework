# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0002_presentations_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='presentations',
            old_name='pub_date',
            new_name='time',
        ),
        migrations.RemoveField(
            model_name='presentations',
            name='text',
        ),
        migrations.AddField(
            model_name='presentations',
            name='title',
            field=models.CharField(default=datetime.datetime(2015, 5, 26, 9, 24, 15, 406000, tzinfo=utc), max_length=70),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='presentations',
            name='id',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
    ]
