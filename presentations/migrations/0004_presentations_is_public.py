# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('presentations', '0003_auto_20150526_1224'),
    ]

    operations = [
        migrations.AddField(
            model_name='presentations',
            name='is_public',
            field=models.BooleanField(default=datetime.datetime(2015, 6, 1, 13, 33, 7, 666000, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
