from django.contrib import admin

from .models import Presentations

class PresentationsAdmin(admin.ModelAdmin):
    list_display = ('name', 'title','is_public')
    list_editable = ('title', 'is_public')
    
    
admin.site.register(Presentations, PresentationsAdmin)
