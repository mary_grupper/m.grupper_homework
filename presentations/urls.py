from django.conf.urls import patterns, url
from presentations import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='presentations_index'),
    url(r'^(?P<presentations_id>\d+)/edit/$', views.edit, name='presentations_edit'),
    url(r'^(?P<presentations_id>\d+)/delete/$', views.delete, name='presentations_delete'),
    url(r'^\d/add/$', views.add, name='presentations_add'),
)
