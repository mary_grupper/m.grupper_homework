from django.db import models

class Presentations(models.Model):
    name = models.CharField(max_length=70)
    title = models.CharField(max_length=70)
    description = models.CharField(max_length=255)
    time = models.DateTimeField()
    is_public = models.BooleanField(default=True, )
    
    def __unicode__(self):
        return self.title 