# -*- coding: utf-8 -*- 
from django import forms
from django.forms import ModelForm
from .models import Presentations

class PresentationsForm(ModelForm):
    class Meta:
        model = Presentations
        fields = ['id','name', 'title', 'description', 'time']
        