from django.shortcuts import render
from .models import Contacts

def index(request):
    contacts_list = Contacts.objects.all()
    context = {'contacts': contacts_list)
    return render(request, 'contacts/index.html', context)