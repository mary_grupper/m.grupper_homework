from django.conf.urls import patterns, include, urls
from django.contrib import admin
urlpatterns = patterns('',
 url(r'^news/',include('news.urls')),
 url(r'^contacts/',include(contacts.urls')),
 url(r'^organizers/',include(organizers.urls')),
 url(r'^partners/',include(partners.urls')),
 url(r'^presentations/',include(presentations.urls')),
 url(r'^speakers/',include(speakers.urls')),
 url(r'^admin/',include(admin.site.urls')),
 )