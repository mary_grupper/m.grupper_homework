from django.conf.urls import patterns, url
from organizers import views
urlpatterns = patterns('',
    url(r'^$', views.index, name='organizers_index'),
)