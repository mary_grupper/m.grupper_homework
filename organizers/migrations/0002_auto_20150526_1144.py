# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizers', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='organizers',
            old_name='description',
            new_name='position',
        ),
        migrations.RemoveField(
            model_name='organizers',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='organizers',
            name='text',
        ),
    ]
