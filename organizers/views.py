from django.shortcuts import render

from .models import Organizers
def index(request):
    organizers_list = Organizers.objects.all()
    context = {'organizers': organizers_list}
    return render(request, 'organizers/index.html', context)
