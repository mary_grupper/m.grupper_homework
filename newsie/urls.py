
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings


urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),
    url(r'^news/', include('news.urls')),
    url(r'^contacts/', include('contacts.urls')),
    url(r'^partners/', include('partners.urls')),
    url(r'^presentations/', include('presentations.urls')),
    url(r'^speakers/', include('speakers.urls')),
    url(r'^organizers/', include('organizers.urls')),
)

from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()